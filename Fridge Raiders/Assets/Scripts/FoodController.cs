﻿using UnityEngine;
using System.Collections;

public class FoodController : MonoBehaviour {

    public float growValue;

    Rigidbody rb;

    public GameObject foodParticles;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            GrowingController grow = other.gameObject.GetComponentInParent<GrowingController>();
            grow.GrowByAmount(growValue, true);
            other.gameObject.GetComponentInParent<PlayerController>().eatParticles.Play();

            Instantiate(foodParticles, transform.position, foodParticles.transform.rotation);

            Destroy(gameObject);
        }
        

    }

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.layer == LayerMask.NameToLayer("LevelGeo"))
        {
            rb.velocity *= 0.5f;
        }
    }
}
