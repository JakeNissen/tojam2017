﻿using UnityEngine;
using System.Collections;

public class GrowingController : MonoBehaviour {

    float scaleIncrement = 0.5f;
    public bool isGoat;
    public bool grow;
    public float elapsed = 0.0f;
    private float duration = 1.0f;
    public Transform belly;
    public Transform head;
    public Transform rightArm;
    public Transform leftArm;
    public Transform leftLeg;
    public Transform rightLeg;
    private float lowerBound;
    public float upperBound;
    float targetScale;

    Rigidbody rb;

    public AudioClip eatting;

    private AudioSource source;

    void Start () {
        rb = GetComponent<Rigidbody>();
        source = GetComponent<AudioSource>();
        grow = false;
        lowerBound = belly.localScale.x;
    }
	
	// Update is called once per frame
	void Update () {
        if(grow)
        {           
                elapsed += Time.deltaTime / duration;
                Vector3 to = new Vector3(targetScale, targetScale, targetScale);
                belly.localScale = Vector3.Lerp(belly.localScale, to, elapsed);

                if (elapsed > 0.15f)
                {
                    belly.localScale = new Vector3(targetScale, targetScale, targetScale);

                if (!isGoat)
                {
                    head.localPosition = new Vector3(0, (belly.localScale.y / 2) / 1.33f, (belly.localScale.z / 2) / 1.33f);
                    rightArm.localPosition = new Vector3((belly.localScale.x / 2), rightArm.localPosition.y, rightArm.localPosition.z);
                    leftArm.localPosition = new Vector3(-(belly.localScale.x / 2), leftArm.localPosition.y, leftArm.localPosition.z);
                    leftLeg.localPosition = new Vector3(leftLeg.localPosition.x, -(belly.localScale.y / 1.90f), leftLeg.localPosition.z);
                    rightLeg.localPosition = new Vector3(rightLeg.localPosition.x, -(belly.localScale.y / 1.90f), rightLeg.localPosition.z);
                }
                    rb.mass = targetScale;


                    grow = false;
                }
            
        }
	}

    public void GrowByAmount(float growAmount, bool growing)
    {
        grow = true;
        elapsed = 0;
        if(growing)
        {
            source.PlayOneShot(eatting, 5.0f);
        }
        scaleIncrement = growAmount;

        targetScale = Mathf.Clamp(belly.localScale.x + scaleIncrement, lowerBound, upperBound);
    }

}
