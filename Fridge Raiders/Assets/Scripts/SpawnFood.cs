﻿using UnityEngine;
using System.Collections;

public class SpawnFood : MonoBehaviour {

    public GameObject food;
    public Transform ring;
    public float minSpawnTime, maxSpawnTime;
    public float thrust;
    public float radius;
    public bool addDirection = false;
    public Quaternion initialRotation;
    public float xOffset;
    public float zOffset;
    private float timeLeft;
    public GameObject[] foods;

    public float initialDelay;


    // Use this for initialization
    void Start () {
        //InvokeRepeating("Spawn", 1f, spawnTime);

        Vector3 lookPos = new Vector3(ring.position.x, transform.position.y, ring.position.z);
        transform.LookAt(lookPos);

        initialRotation = transform.rotation;
	}
	
    void Update()
    {
        timeLeft -= Time.deltaTime;
        if(timeLeft < 0 && Time.time - GameManager.instance.gameStartTime > initialDelay)
        {
            GameObject clone = Instantiate(foods[(int)Random.Range(0,foods.Length)], gameObject.transform.position, gameObject.transform.rotation) as GameObject;
            clone.transform.Rotate(new Vector3(Random.Range(0, 360), Random.Range(0, 360), Random.Range(0, 360)));
            Rigidbody rb = clone.GetComponent<Rigidbody>();
            float random = Random.Range(0, 360);
            if (transform.position.z != 0)
            {
                float xCircle = ring.position.x + (Random.Range(0, radius * 0.9f) * Mathf.Cos(random * Mathf.PI / 180));
                float zCircle = ring.position.z + (Random.Range(0, radius * 0.9f) * Mathf.Sin(random * Mathf.PI / 180));
                float adjacent = zCircle - transform.position.z;
                float opposite = xCircle - transform.position.x;
                //print(xCircle + " " + zCircle);
                Debug.DrawRay(new Vector3(xCircle, 0, zCircle), Vector3.up * 5, Color.red, 10f);

                float velocity = (Mathf.Sqrt(Mathf.Abs(adjacent * zOffset) * Mathf.Abs(Physics.gravity.y)) / Mathf.Sin(45 * 2 * Mathf.PI / 180));
                transform.LookAt(new Vector3(xCircle * xOffset, transform.position.y, zCircle));
                transform.Rotate(new Vector3(-45, 0, 0));
                rb.velocity = transform.forward * velocity;

                Vector3 velocityVector = new Vector3(0, velocity * Mathf.Sin(45 * Mathf.PI / 180), velocity * Mathf.Cos(random * Mathf.PI / 180));

                transform.rotation = initialRotation;
                timeLeft = Random.Range(minSpawnTime, maxSpawnTime);
            } else
            {
                float zCircle = ring.position.x + (Random.Range(0, radius * 0.9f) * Mathf.Cos(random * Mathf.PI / 180));
                float xCircle = ring.position.z + (Random.Range(0, radius * 0.9f) * Mathf.Sin(random * Mathf.PI / 180));
                float adjacent = zCircle - transform.position.x;
                float opposite = xCircle - transform.position.z;
                //print(xCircle + " " + zCircle);
                Debug.DrawRay(new Vector3(xCircle, 0, zCircle), Vector3.up * 5, Color.red, 10f);

                float velocity = (Mathf.Sqrt(Mathf.Abs(adjacent * zOffset) * Mathf.Abs(Physics.gravity.y)) / Mathf.Sin(45 * 2 * Mathf.PI / 180));
                transform.LookAt(new Vector3(xCircle * xOffset, transform.position.y, zCircle));
                transform.Rotate(new Vector3(-45, 0, 0));
                rb.velocity = transform.forward * velocity;

                Vector3 velocityVector = new Vector3(0, velocity * Mathf.Sin(45 * Mathf.PI / 180), velocity * Mathf.Cos(random * Mathf.PI / 180));

                transform.rotation = initialRotation;
                timeLeft = Random.Range(minSpawnTime, maxSpawnTime);
            }
           
        }
    }

    void Spawn() {
        
    }
}
    