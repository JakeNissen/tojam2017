﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerController : MonoBehaviour
{

    public int playerID;


    public string controlSuffix;
    public string horizontalAxisName, verticalAxisName, boostButtonName;
    public Transform belly;
    public float baseForce, maxForce;

    float currentForce;

    public float forceAccel;

    Vector3 inputVector;

    public float frictionCoefficient;

    Vector3 movementDirection;

    public float brakeAngle;

    Rigidbody rb;

    private GrowingController grow;
    public float weightLoss;

    public float bumpThreshold;
    public float bumpPower;
    public float maxBumpPower;

    float lastBumpTime;

    public float bumpCooldown;

    public float obstacleVelocityMultiplier;

    public float boostForce;

    Vector3 forceVector;

    bool boostQueued;

    public float boostCoolDown;
    private float timeBeforeBoost;

    public bool loseWeightOnHit;
    public float weightLossForceRequired;
    public float weightLossValue;

    public bool loseWeightBoost;

    Vector3[] velocityHistory = new Vector3[2];

    public GameObject bumpParticles;

    public GameObject shrinkParticles;

    public ParticleSystem fartParticles;


    public AudioClip[] farts;
    public AudioClip[] bumps;


    private AudioSource source;

    public ParticleSystem eatParticles;

    public MeshRenderer bellyMesh;

    public MeshRenderer[] limbMeshes;


    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        source = GetComponent<AudioSource>();
        timeBeforeBoost = 0;
        //rb.maxAngularVelocity = maxAngularVelocity;
        grow = GetComponent<GrowingController>();
        GameManager.instance.players.Add(this);
    }

    // Update is called once per frame
    void Update()
    {

        timeBeforeBoost += Time.deltaTime;
        UpdatePlayerIntent();
        SimulatePlayerMovement();

    }

    void FixedUpdate()
    {


        //print("Velocity: " + rb.velocity + ", " + rb.velocity.magnitude);
        //print("Angular Velocity: " + rb.angularVelocity + ", " + rb.angularVelocity.magnitude);
    }

    void UpdatePlayerIntent()
    {
        float xInput = 0;
        float yInput = 0;

        if (GameManager.instance.gameState == GameManager.GameState.Gameplay || GameManager.instance.winner == playerID)
        {
            // Take Controller/Keyboard Input from appropriate Player
            xInput = Input.GetAxis(horizontalAxisName + controlSuffix);
            yInput = Input.GetAxis(verticalAxisName + controlSuffix);
        }

        movementDirection = rb.velocity.normalized;

        inputVector = new Vector3(xInput, 0, yInput);

        if (Vector3.Angle(movementDirection, new Vector3(xInput, 0, yInput)) > brakeAngle){
            currentForce = baseForce;
        }else
        {
            currentForce = Mathf.Clamp(currentForce + forceAccel * Time.deltaTime, baseForce * inputVector.normalized.magnitude, maxForce * inputVector.normalized.magnitude);
        }

        forceVector = inputVector * currentForce;

        /*if (Vector3.Angle(movementDirection, new Vector3(xInput, 0, yInput)) > brakeAngle){
            rb.velocity = new Vector3(rb.velocity.x * 0.9f, rb.velocity.y, rb.velocity.z * 0.9f);
            rb.angularVelocity *= 0.75f;
        }*/



        if (Input.GetButtonDown(boostButtonName + controlSuffix) && (GameManager.instance.gameState == GameManager.GameState.Gameplay || GameManager.instance.winner == playerID))
        {
            boostQueued = true;
        }


    }

    void SimulatePlayerMovement()
    {

        rb.velocity = new Vector3(Mathf.MoveTowards(rb.velocity.x, 0, Mathf.Abs(rb.velocity.x) * frictionCoefficient * Time.deltaTime), rb.velocity.y, Mathf.MoveTowards(rb.velocity.z, 0, Mathf.Abs(rb.velocity.z) * frictionCoefficient * Time.deltaTime));
        rb.angularVelocity = new Vector3(Mathf.MoveTowards(rb.angularVelocity.x, 0, Mathf.Abs(rb.angularVelocity.x) * frictionCoefficient * Time.deltaTime), Mathf.MoveTowards(rb.angularVelocity.y, 0, Mathf.Abs(rb.angularVelocity.y) * frictionCoefficient * Time.deltaTime), Mathf.MoveTowards(rb.angularVelocity.z, 0, Mathf.Abs(rb.angularVelocity.z) * frictionCoefficient * Time.deltaTime));

        rb.AddForce(forceVector, ForceMode.Impulse);

        Vector3 idealVelocity = rb.velocity.magnitude * inputVector;

        rb.velocity = Vector3.Lerp(rb.velocity, idealVelocity, 0.5f * Time.deltaTime);

        if (boostQueued && timeBeforeBoost >= boostCoolDown)
        {
            source.PlayOneShot(farts[Random.Range(0, farts.Length)], 1.0f);
            if(loseWeightBoost)
            {
                grow.GrowByAmount(weightLoss, false);
            }

            rb.AddForce(inputVector * boostForce, ForceMode.VelocityChange);
            boostQueued = false;
            timeBeforeBoost = 0;

            fartParticles.Play();

        } else
        {
            boostQueued = false;
        }

        velocityHistory[1] = velocityHistory[0];
        velocityHistory[0] = rb.velocity;

    }

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Player")
        {
            if (rb.velocity.magnitude >= bumpThreshold && Time.time - lastBumpTime > bumpCooldown && Vector3.Angle(movementDirection, col.transform.position - transform.position) < 60)
            {

                if (loseWeightOnHit)
                {
                    print(rb.velocity.magnitude);
                    if (velocityHistory[1].magnitude >= weightLossForceRequired && timeBeforeBoost < boostCoolDown)
                    {
                        GrowingController grower = col.gameObject.GetComponentInParent<GrowingController>();
                        grower.GrowByAmount(weightLossValue, false);
                        Instantiate(shrinkParticles, col.transform.position, shrinkParticles.transform.rotation);
                    }
                }
                Rigidbody otherRb = col.gameObject.GetComponent<Rigidbody>();

                Vector3 direction = col.transform.position - transform.position;

                direction.Normalize();

                otherRb.AddForce(direction * Mathf.Clamp(rb.velocity.magnitude * bumpPower, 0, maxBumpPower), ForceMode.Impulse);

                lastBumpTime = Time.time;

                Instantiate(bumpParticles, col.contacts[0].point, Quaternion.identity);
                source.PlayOneShot(bumps[Random.Range(0, bumps.Length)], 0.5f);
            }
         
        }

        if (col.gameObject.tag == "Obstacle")
        {
            Vector3 direction = col.contacts[0].point - transform.position;

            direction.Normalize();

            rb.AddForce(-direction * velocityHistory[1].magnitude * obstacleVelocityMultiplier, ForceMode.Impulse);
        }
    }
}