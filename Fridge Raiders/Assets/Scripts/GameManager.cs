﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    public GameObject playerPrefab;
    public GameObject goatPrefab;

    [Range(0f,1f)]
    public float goatChance;

    bool spawnedAGoat;

    public Transform sumoCircle;
    public float sumoCircleRadius;

    static GameManager _instance;

    [HideInInspector]
    public SceneRefs sceneRefs;

    public static GameManager instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<GameManager>();
            }
            return _instance;
        }
    }

    public enum GameState { Menu, PreGame, Gameplay, PostGame }

    public GameState gameState;

    public List<PlayerController> players;

    public List<int> playersInCircle;

    public int numOfPlayers;

    bool oneRemainsInCircle = false;

    float winTimerStart;

    public float winTimer;

    [HideInInspector]
    public int winner;

    bool gameEnded;

    [HideInInspector]
    public UIMethods ui;

    public Material[] bellyMaterials;
    public Material[] skinMaterials;

    public float gameStartTime;

    void Awake()
    {
        if (instance != this)
        {
            GameObject.Destroy(gameObject);
            return;
        }
    }
	
	// Update is called once per frame
	void Update () {

        if (gameState == GameState.Menu)
        {
            if (gameEnded)
            {
                players.Clear();
                playersInCircle.Clear();

                ui.gameObject.GetComponent<ShowPanels>().ShowMenu();

                gameEnded = false;
            }
        }

        if (gameState == GameState.PreGame)
        {
            Scene currentScene = SceneManager.GetActiveScene();

            if (currentScene.buildIndex == 1 || currentScene.buildIndex == 2)
            {
                sceneRefs = GameObject.Find("SceneRefs").GetComponent<SceneRefs>();
                sumoCircle = sceneRefs.sumoCircle;

                for (int i = 0; i < numOfPlayers; i++)
                {
                    float rand = Random.Range(0f, 1f);

                    if (rand <= goatChance && !spawnedAGoat)
                    {
                        GameObject playerClone = Instantiate(goatPrefab, sceneRefs.spawnPoints[i].position, sceneRefs.spawnPoints[i].rotation) as GameObject;
                        PlayerController cloneController = playerClone.GetComponent<PlayerController>();

                        cloneController.playerID = i + 1;
                        cloneController.controlSuffix = cloneController.playerID.ToString();

                        spawnedAGoat = true;
                    }
                    else
                    {

                        GameObject playerClone = Instantiate(playerPrefab, sceneRefs.spawnPoints[i].position, sceneRefs.spawnPoints[i].rotation) as GameObject;
                        PlayerController cloneController = playerClone.GetComponent<PlayerController>();

                        cloneController.playerID = i + 1;
                        cloneController.controlSuffix = cloneController.playerID.ToString();

                        cloneController.bellyMesh.material = bellyMaterials[i];
                        for (int j = 0; j < cloneController.limbMeshes.Length; j++)
                        {
                            cloneController.limbMeshes[j].material = skinMaterials[i];
                        }
                    }
                }

                ui.instructionsOverlay.SetActive(false);
                gameStartTime = Time.time;

                gameState = GameState.Gameplay;
            }
        }

        if (gameState == GameState.Gameplay)
        {

            playersInCircle.Clear();

            for (int i = 0; i < players.Count; i++)
            {

                Vector3 playerPos = new Vector3(players[i].transform.position.x, sumoCircle.position.y, players[i].transform.position.z);

                if (Vector3.Distance(playerPos, sumoCircle.position) < sumoCircleRadius)
                {
                    playersInCircle.Add(players[i].playerID);
                }
            }

            if (playersInCircle.Count == 1 && !oneRemainsInCircle)
            {
                oneRemainsInCircle = true;
                winTimerStart = Time.time;
            }
            else if (playersInCircle.Count != 1)
            {
                oneRemainsInCircle = false;
            }

            if (oneRemainsInCircle && Time.time - winTimerStart > winTimer)
            {
                winner = playersInCircle[0];
                gameEnded = true;
                ui.postGameOverlay.SetActive(true);
                ui.winnerText.text = "Player " + winner + " wins!";
                gameState = GameState.PostGame;
            }

        }

        if (gameState == GameState.PostGame)
        {

            if (Input.GetButtonDown("Submit"))
            {
                gameState = GameState.Menu;

                ui.postGameOverlay.SetActive(false);

                spawnedAGoat = false;

                SceneManager.LoadScene(0);
            }
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Quit();
        }
	}

    public void Quit()
    {
        //If we are running in a standalone build of the game
#if UNITY_STANDALONE
        //Quit the application
        Application.Quit();
#endif

        //If we are running in the editor
#if UNITY_EDITOR
        //Stop playing the scene
        UnityEditor.EditorApplication.isPlaying = false;
#endif
    }


}
