﻿using UnityEngine;
using System.Collections;

public class SceneRefs : MonoBehaviour {

    public Transform sumoCircle;

    public Transform[] spawnPoints;

    void Start() {
        GameManager.instance.sceneRefs = this;
    }
}
