﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIMethods : MonoBehaviour {

    static UIMethods _instance;

    public static UIMethods instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<UIMethods>();
            }
            return _instance;
        }
    }


    public Text playerNumText;

    public GameObject postGameOverlay;

    public Text winnerText;

    public GameObject instructionsOverlay;

    void Awake()
    {
        if (instance != this)
        {
            GameObject.Destroy(gameObject);
            return;
        }
    }

	// Use this for initialization
	void Start () {
        GameManager.instance.ui = this;
	}
	

    public void IncrementNumberOfPlayers()
    {
        GameManager.instance.numOfPlayers += 1;

        if (GameManager.instance.numOfPlayers > 4)
        {
            GameManager.instance.numOfPlayers = 2;
        }

        playerNumText.text = "Players: " + GameManager.instance.numOfPlayers;
    }

    public void ToggleInstructionsGraphic()
    {
        instructionsOverlay.SetActive(!instructionsOverlay.activeInHierarchy);
    }
}
