﻿using UnityEngine;
using System.Collections;

public class DestroyAfterTime : MonoBehaviour {

    public float time;

	// Use this for initialization
	void Start () {
        Destroy(gameObject, time);
	}
}
